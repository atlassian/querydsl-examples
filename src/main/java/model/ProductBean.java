package model;

import java.util.Date;

public class ProductBean {
    private Long id;
    private String name;
    private Date launchDate;

    public ProductBean(Long id, String name, Date launchDate) {
        this.id = id;
        this.name = name;
        this.launchDate = launchDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getLaunchDate() {
        return launchDate;
    }

    public void setLaunchDate(Date launchDate) {
        this.launchDate = launchDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
