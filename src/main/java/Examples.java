import com.querydsl.core.group.GroupBy;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.sql.PostgreSQLTemplates;
import com.querydsl.sql.SQLExpressions;
import com.querydsl.sql.SQLQuery;
import com.querydsl.sql.dml.SQLInsertClause;
import com.querydsl.sql.dml.SQLUpdateClause;
import model.CommentDTO;
import model.PostDTO;
import model.ProductBean;
import model.ProductDTO;
import org.junit.Test;
import tables.QComment;
import tables.QEmployee;
import tables.QPost;
import tables.QProduct;
import tables.QProductAvailability;
import tables.QProductItem;
import tables.QProductItemSku;

import java.sql.Connection;
import java.util.List;
import java.util.Map;

/**
 * The idea behind this examples class is just to be a holder of compiled code, not runnable
 *
 * Put the example you want in here and make sure it compiles before referencing it in the README
 *
 * if you know if a way to reference examples in READMEs - tell me
 */
@SuppressWarnings({"unused", "WeakerAccess", "ConfusingArgumentToVarargsMethod", "ConstantConditions", "ConstantIfStatement"})
public class Examples {
    QProduct PRODUCT = new QProduct();
    QProductItem PRODUCT_ITEM = new QProductItem();
    QProductItemSku PRODUCT_ITEM_SKU = new QProductItemSku();
    QProductAvailability PRODUCT_AVAILIBILITY = new QProductAvailability();
    QPost POST = new QPost();
    QComment COMMENT = new QComment();

    @Test
    public void example_join() throws Exception {
        render("join",
                query()
                        .select(PRODUCT_ITEM_SKU.SKU_BARCODE, PRODUCT_ITEM.PRICE, PRODUCT.NAME)
                        .from(PRODUCT)
                        .join(PRODUCT_ITEM)
                        .on(PRODUCT_ITEM.PRODUCT_ID.eq(PRODUCT.ID))
                        .join(PRODUCT_ITEM_SKU)
                        .on(PRODUCT_ITEM_SKU.ID.eq(PRODUCT_ITEM.SKU_ID))
                        .orderBy(PRODUCT_ITEM.PRICE.asc()));
    }

    @Test
    public void example_left_join() throws Exception {
        render("left_join",
                query()
                        .select(PRODUCT_ITEM_SKU.SKU_BARCODE, PRODUCT_ITEM.PRICE, PRODUCT.NAME)
                        .from(PRODUCT)
                        .leftJoin(PRODUCT_ITEM)
                        .on(PRODUCT_ITEM.PRODUCT_ID.eq(PRODUCT.ID))
                        .orderBy(PRODUCT_ITEM.PRICE.asc())
        );


    }

    @Test
    public void example_subquery() {

        render("SQLExpressions.select",
                query()
                        .select(PRODUCT_ITEM_SKU.SKU_BARCODE, PRODUCT_ITEM.PRICE, PRODUCT.NAME)
                        .from(PRODUCT)
                        .join(PRODUCT_ITEM)
                        .on(PRODUCT_ITEM.PRODUCT_ID.eq(PRODUCT.ID))
                        .join(PRODUCT_ITEM_SKU)
                        .on(PRODUCT_ITEM_SKU.ID.eq(PRODUCT_ITEM.SKU_ID))
                        .where(
                                PRODUCT.NAME.like("drone%")
                                        .and(PRODUCT.ID.in(
                                                SQLExpressions.select(PRODUCT.ID)
                                                        .from(PRODUCT)
                                                        .join(PRODUCT_AVAILIBILITY)
                                                        .on(PRODUCT_AVAILIBILITY.PRODUCT_ID.eq(PRODUCT.ID))
                                                        .where(PRODUCT_AVAILIBILITY.AVAILABLE.eq(true))
                                        )))
                        .orderBy(PRODUCT_ITEM.PRICE.asc()));

    }

    @Test
    public void example_aliasing() throws Exception {
        // note that the Q entities are instantiated with aliases in this case
        QEmployee WORKER = new QEmployee("worker");
        QEmployee MANAGER = new QEmployee("manager");
        render("aliasing",
                query()
                        .select(WORKER.NAME.as("worker_name"), MANAGER.NAME.as("manager_name"))
                        .from(WORKER)
                        .join(MANAGER)
                        .on(MANAGER.ID.eq(WORKER.MANAGER_ID))
        );
    }

    @Test
    public void select_for_update() throws Exception {
        render("select for update",
                query()
                        .select(PRODUCT.all())
                        .forUpdate()
                        .from(PRODUCT)
                        .orderBy(PRODUCT.NAME.asc())
        );
    }

    @Test
    public void select_current_time() throws Exception {
        render("select time expressions",
                query()
                        .select(PRODUCT.LAUNCH_DATE, Expressions.currentTime(), Expressions.currentTimestamp())
                        .from(PRODUCT)
                        .where(PRODUCT.LAUNCH_DATE.before(Expressions.currentDate()))
        );
    }

    @Test
    public void select_maths_max_expression() throws Exception {
        render("select max expressions",
                query()
                        .select(PRODUCT_ITEM.PRICE.max())
                        .from(PRODUCT_ITEM)
        );
    }

    @Test
    public void constructor_projection() throws Exception {
        if (false) {
            List<ProductDTO> result = query()
                    .select(Projections.constructor(
                            ProductDTO.class, PRODUCT.ID, PRODUCT.NAME, PRODUCT.LAUNCH_DATE)
                    )
                    .from(PRODUCT)
                    .fetch();

        }
        render("constructor_projection",
                query()
                        .select(Projections.constructor(
                                ProductDTO.class, PRODUCT.ID, PRODUCT.NAME, PRODUCT.LAUNCH_DATE)
                        )
                        .from(PRODUCT)
        );
    }

    @Test
    public void bean_projection() throws Exception {
        if (false) {
            List<ProductBean> result = query()
                    .select(Projections.bean(PRODUCT,
                            PRODUCT.ID, PRODUCT.NAME, PRODUCT.LAUNCH_DATE)
                    )
                    .from(PRODUCT)
                    .fetch();

        }
        render("bean_projection",
                query()
                        .select(Projections.bean(PRODUCT,
                                PRODUCT.ID, PRODUCT.NAME, PRODUCT.LAUNCH_DATE)
                        )
                        .from(PRODUCT)
        );
    }

    @Test
    public void group_by_projection() throws Exception {

        if (false) {
            Map<Long, PostDTO> post = query()
                    .from(POST)
                    .join(COMMENT).on(POST.ID.eq(COMMENT.POST_ID))
                    .transform(GroupBy.groupBy(POST.ID).as(
                            Projections.bean(PostDTO.class, POST.ID, POST.TITLE, POST.CONTENT,
                                    GroupBy.set(COMMENT).as("comments"))
                            )
                    );
        }

        render("groupby_projection",
                query()
                        .select(POST.all())
                        .from(POST)
                        .join(COMMENT).on(POST.ID.eq(COMMENT.POST_ID)));
    }

    @Test
    public void group_by_list_projection() throws Exception {

        if (false) {
            Map<Long, List<CommentDTO>> result = query()
                    .from(POST)
                    .join(COMMENT).on(POST.ID.eq(COMMENT.POST_ID))
                    .transform(GroupBy.groupBy(POST.ID).as(
                            GroupBy.list(COMMENT))
                    );
        }

        render("group_by_list_projection",
                query()
                        .select(POST.all())
                        .from(POST)
                        .join(COMMENT).on(POST.ID.eq(COMMENT.POST_ID)));
    }

    @Test
    public void simple_insert() throws Exception {
        Connection connection = null;
        if (false) {
            SQLInsertClause insertClause = new SQLInsertClause(connection, PostgreSQLTemplates.DEFAULT, POST);
            insertClause.set(POST.CONTENT, "content")
                    .set(POST.TITLE, "title");
        }

        SQLInsertClause insertClause = new SQLInsertClause(connection, PostgreSQLTemplates.DEFAULT, POST);
        insertClause.set(POST.CONTENT, "content")
                .set(POST.TITLE, "title");
        render("simple_insert", insertClause.toString());
    }

    @Test
    public void simple_insert_with_key() throws Exception {
        Connection connection = null;
        if (false) {
            SQLInsertClause insertClause = new SQLInsertClause(connection, PostgreSQLTemplates.DEFAULT, POST);
            Integer generatedKey = insertClause.set(POST.CONTENT, "content")
                    .set(POST.TITLE, "title")
                    .executeWithKey(Integer.class);
        }

        SQLInsertClause insertClause = new SQLInsertClause(connection, PostgreSQLTemplates.DEFAULT, POST);
        insertClause.set(POST.CONTENT, "content")
                .set(POST.TITLE, "title");
        render("simple_insert", insertClause.toString());
    }

    @Test
    public void simple_update() throws Exception {
        Connection connection = null;
        if (false) {
            SQLUpdateClause updateClause = new SQLUpdateClause(connection, PostgreSQLTemplates.DEFAULT, POST);
            updateClause.set(POST.CONTENT, "updated content")
                    .set(POST.TITLE, "updated title")
                    .where(POST.ID.eq(1l));
        }

        SQLUpdateClause updateClause = new SQLUpdateClause(connection, PostgreSQLTemplates.DEFAULT, POST);
        updateClause.set(POST.CONTENT, "updated content")
                .set(POST.TITLE, "updated title")
                .where(POST.ID.eq(1l));
        render("simple_update", updateClause.toString());
    }

    @Test
    public void simple_update_against_database_value() throws Exception {
        Connection connection = null;
        if (false) {
            SQLUpdateClause updateClause = new SQLUpdateClause(connection, PostgreSQLTemplates.DEFAULT, POST);
            updateClause
                    .set(POST.ID, POST.ID.add(5))
                    .where(POST.ID.eq(1l));
        }

        SQLUpdateClause updateClause = new SQLUpdateClause(connection, PostgreSQLTemplates.DEFAULT, POST);
        updateClause
                .set(POST.ID, POST.ID.add(5))
                .where(POST.ID.eq(1l));
        render("simple_update_against_database_value", updateClause.toString());
    }

    private String render(String name, SQLQuery<?> sql) {
        return render(name, sql.toString());
    }

    private String render(String name, String sql) {
        String s = sql.toString();
        System.out.println();
        System.out.println(name);
        System.out.println("-----");
        System.out.println(s);
        System.out.println("-----");
        System.out.println();
        return s;
    }

    private SQLQuery<?> query() {
        return new SQLQuery(PostgreSQLTemplates.DEFAULT);
    }

}
