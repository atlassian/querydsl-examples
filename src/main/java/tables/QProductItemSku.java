package tables;

import com.atlassian.pocketknife.spi.querydsl.EnhancedRelationalPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;

public class QProductItemSku extends EnhancedRelationalPathBase<QProductItemSku> {

    public NumberPath<Long> ID = createLongCol("ID").asPrimaryKey().build();
    public StringPath SKU_STICKER = createStringCol("SKU_STICKER").build();
    public StringPath SKU_BARCODE = createStringCol("SKU_BARCODE").build();

    public QProductItemSku() {
        super(QProductItemSku.class, "PRODUCT_ITEM_SKU");
    }
}
