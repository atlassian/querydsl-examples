package tables;

import com.atlassian.pocketknife.spi.querydsl.EnhancedRelationalPathBase;
import com.querydsl.core.types.dsl.BooleanPath;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.NumberPath;

import java.sql.Timestamp;

public class QProductAvailability extends EnhancedRelationalPathBase<QProductAvailability> {

    public NumberPath<Long> ID = createLongCol("ID").asPrimaryKey().build();
    public NumberPath<Long> PRODUCT_ID = createLongCol("PRODUCT_ID").build();
    public BooleanPath AVAILABLE = createBooleanCol("AVAILABLE").build();
    public DateTimePath<Timestamp> AVAILABLE_SINCE = createDateTimeCol("AVAILABLE_SINCE", Timestamp.class).build();

    public QProductAvailability() {
        super(QProductAvailability.class, "PRODUCT_AVAILABILITY");
    }
}
