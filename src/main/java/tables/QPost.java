package tables;

import com.atlassian.pocketknife.spi.querydsl.EnhancedRelationalPathBase;
import com.querydsl.core.types.dsl.DatePath;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import model.PostDTO;
import model.ProductBean;

import java.util.Date;


public class QPost extends EnhancedRelationalPathBase<PostDTO> {

    public NumberPath<Long> ID = createLongCol("ID").asPrimaryKey().build();
    public StringPath TITLE = createStringCol("TITLE").build();
    public StringPath CONTENT = createStringCol("CONTENT").build();

    public QPost() {
        super(PostDTO.class, "POST");
    }
}
